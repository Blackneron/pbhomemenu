# PBhomemenu Script - README #
---

### Overview ###

The **PBhomemenu** script (PHP) is used to display various link buttons. The script can be defined as the home screen in your favorite browser. The menu page is fully responsive and optimized for speed (minimized code). From the menu page it is possible to add new links (URLs), delete existing ones and open all the URLs in a new browser window. The data of the script is saved in a JSON file (in the same directory where the script resides) and will be cached by the browser. The **PBhomemenu** script does not need any database like MySQL, frameworks like jQuery or Bootstrap and can installed very easy (1 minute) on a host which supports PHP. There is just one file to upload and you can customize the configuration section.

### Screenshots ###

![PBhomemenu - Start](development/readme/pbhomemenu_1.png "PBhomemenu - Start")
![PBhomemenu - Add Link](development/readme/pbhomemenu_2.png "PBhomemenu - Add Link")
![PBhomemenu - Delete Link](development/readme/pbhomemenu_3.png "PBhomemenu - Delete Link")
![PBhomemenu - Tablet View](development/readme/pbhomemenu_4.png "PBhomemenu - Tablet View")
![PBhomemenu - Mobile View](development/readme/pbhomemenu_5.png "PBhomemenu - Mobile View")

### Setup ###

* Copy the directory **pbhomemenu** to your webhost (webdirectory).
* Set the permissions for files to **644** and for directories to **755**.
* Make sure that the directory **pbhomemenu** is writable by the PHP/Webserver process.
* Permissions may vary, depending on the owner of the files and the running webserver process.
* Edit the configuration section of the file **pbhomemenu/menu.php**.
* Access the menu file **https://<YOUR_DOMAIN>/pbhomemenu/menu.php** from your webbrowser.
* In the footer there are links to access the **new/add** or **delete** page.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PBhomemenu** script is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
