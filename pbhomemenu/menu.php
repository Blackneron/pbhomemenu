<?php

// ----0--9--8--7--6--5--4--3--2--1--1--2--3--4--5--6--7--8--9--0---- //
// ================================================================== //
//                                                                    //
//                       PBhomemenu - Home Menu                       //
//                                                                    //
// ================================================================== //
//                                                                    //
//                      Version 1.2 / 15.04.2020                      //
//                                                                    //
//                      Copyright 2020 - PB-Soft                      //
//                                                                    //
//                             pb-soft.com                            //
//                                                                    //
//                           Patrick Biegel                           //
//                                                                    //
// ================================================================== //

// =====================================================================
//              C O N F I G U R A T I O N   -   S T A R T
// =====================================================================

// Please specify the URL password.
$password = "matterhorn";

// Please specify the minimum width of the link boxes (in pixel).
$min_width = 300;

// Please specify the name and path of the data file.
$data_file = "links_".pathinfo(__FILE__, PATHINFO_FILENAME).".json";

// Please specify if the search input field is enabled.
$search = 1;

// Please specify if the 'new/add' mode is enabled.
$enable_add = 0;

// Please specify if the 'delete' mode is enabled.
$enable_delete = 0;

// Please specify if the backup link is enabled.
$enable_backup = 0;

// Please specify if the menu links should be opened in a new tab.
$new_window = 1;

// Please specify if the links should be sorted alphabetically.
$sort_links = 1;


// =====================================================================
//                C O N F I G U R A T I O N   -   E N D
// =====================================================================

// =====================================================================
// Initialization
// =====================================================================

// Specify the company name.
$company = "PB-Soft";

// Specify the company website.
$website = "https://pb-soft.com";

// Specify the application name.
$application = "PBhomemenu";

// Specify the application version.
$version = "1.2";

// Specify if the copyright is displayed.
$display_copyright = 1;

// Specify an example entry for the links array.
$example_name = "Duckduckgo";
$example_url = "https://duckduckgo.com";

// Specify the used colors.
$orange = "#ff9800";
$green = "#96e4a1";
$blue = "#040515";
$red = "#b12630";

// Initialize the background color.
$background = $blue;

// Initialize the password.
$pass = "";

// Set defaults for the 'add' and 'delete' variables.
$add = 0;
$delete = 0;


// =====================================================================
// Get data from JSON file.
// =====================================================================

// Check if the JSON data file is writable.
if (is_writable($data_file)) {

  // Read the JSON data from the file.
  $links = read_data($data_file, $sort_links);

  // Check if the file could be read.
  if (is_array($links)) {


    // =================================================================
    // Insert example data if the links array is empty.
    // =================================================================

    // Check if the links array does not contain any data.
    if (count($links) == 0) {

      // Insert an example into the links array.
      $links[$example_name] = $example_url;

      // Save the links array to the JSON file.
      $status = save_data($links, $data_file);

      // Check if the write operation has failed.
      if ($status > 0) {

        // Set the background to an error color.
        $background = $red;
      }
    }


    // =================================================================
    // Specify link targets.
    // =================================================================

    // Check if the links should be opened in a new tab/window.
    if ($new_window == 1) {

      // Specify the link target.
      $target = "_blank";

      // The links should be opened in the same window.
    } else {

      // Specify the link target.
      $target = "_self";
    }


    // =================================================================
    // Check password GET / POST.
    // =================================================================

    // Check if a password was submitted via GET and is not empty.
    if (isset($_GET['pass']) && trim($_GET['pass']) != "") {

      // Save the parameter into the 'pass' variable.
      $pass = trim(urldecode($_GET['pass']));
    }

    // Check if a password was submitted via POST and is not empty.
    if (isset($_POST['pass']) && trim($_POST['pass']) != "") {

      // Save the parameter into the 'pass' variable.
      $pass = trim($_POST['pass']);
    }

    // Check the submitted password.
    if ($pass == trim($password)) {

      // Enable the 'add' flag for access with password.
      $enable_add = 1;

      // Enable the 'delete' flag for access with password.
      $enable_delete = 1;

      // Enable the 'backup' flag for access with password.
      $enable_backup = 1;
    }


    // =================================================================
    // Check link name and link URL - GET.
    // =================================================================

    // Check if the link name was submitted and is not empty.
    if (isset($_GET['name']) && trim($_GET['name']) != "") {

      // Save the parameter into the 'name' variable.
      $name = trim(urldecode($_GET['name']));

      // Check if the URL was submitted and is not empty.
      if (isset($_GET['url']) && trim($_GET['url']) != "") {

        // Save the parameter into the 'url' variable.
        $url = trim(urldecode($_GET['url']));
      }
    }


    // =================================================================
    // Check 'delete' mode - GET.
    // =================================================================

    // Check if the 'delete' mode is enabled.
    if ($enable_delete == 1) {

      // Check if the 'delete' parameter was set and is 1.
      if (isset($_GET['delete']) && $_GET['delete'] == 1) {

        // Enable the 'delete' flag.
        $delete = 1;

        // Specify the background color for the 'delete' mode.
        $background = $orange;

        // Check if the link name and URL was specified.
        if (isset($name) && isset($url)) {

          // Search for the URL in the links array and get the key.
          $key = array_search($url, $links, true);

          // Check if the URL was found in the links array.
          if ($key !== false) {

            // Delete the actual item from the links array.
            unset($links[$key]);

            // Check if the URL still exists in the links array.
            if (array_key_exists($url, $links)) {

              // Set the background to an error color.
              $background = $red;

              // The URL does not exist anymore.
            } else {

              // Save the links array to the JSON file.
              $status = save_data($links, $data_file);

              // Check if the write operation was successful.
              if ($status == 0) {

                // Set the background to a success color.
                $background = $green;

                // The write operation has failed.
              } else {

                // Set the background to an error color.
                $background = $red;
              }
            } // The URL does not exist anymore.
          } // Check if the URL was found in the links array.
        } // Check if the link name and URL was specified.
      } // Check if the 'delete' parameter was set and is 1.
    } // Check if the 'delete' mode is enabled.


    // =================================================================
    // Check 'new/add' mode - GET / POST.
    // =================================================================

    // Check if the 'new/add' mode is enabled.
    if ($enable_add == 1) {

      // Check if the 'add' parameter was set and is 1.
      if (isset($_GET['add']) && $_GET['add'] == 1) {

        // Enable the 'add' flag.
        $add = 1;
      }

      // Check if the link name was submitted and is not empty.
      if (isset($_POST['name']) && trim($_POST['name']) != "") {

         // Enable the 'add' flag to enter another new link.
        $add = 1;

        // Save the parameter into the 'name' variable.
        $name = trim($_POST['name']);

        // Check if the URL was submitted and is not empty.
        if (isset($_POST['url']) && trim($_POST['url']) != "") {

          // Save the parameter into the 'url' variable.
          $url = trim($_POST['url']);

          // Check if the URL ends with a '/' slash.
          if (substr($url, -1) == "/") {

            // Specify the check URL without slash.
            $check_url = substr($url, 0, -1);

            // The URL does not end with a '/' slash.
          } else {

            // Set the check URL to the submitted URL.
            $check_url = $url;
          }

          // Check if the URL already does exist in the links array.
          if (in_array($url, $links) || in_array($check_url, $links)) {

            // Set the background to a warning color.
            $background = $orange;

            // The new URL does not exist.
          } else {

            // Insert the new URL into the links array.
            $links[$name] = $url;

            // Save the links array to the JSON file.
            $status = save_data($links, $data_file);

            // Check if the write operation was successful.
            if ($status == 0) {

              // Set the background to a success color.
              $background = $green;

              // The data could not be written to the file.
            } else {

              // Set the background to an error color.
              $background = $red;
            }
          } // The new URL does not exist.
        } // Check if the URL was submitted and is not empty.
      } // Check if the link name was submitted and is not empty.
    } // Check if the 'new/add' mode is enabled.


    // =================================================================
    // Add the HTML header.
    // =================================================================

    // Add the HTML header to the HTML output.
    $output  = "<!DOCTYPE html>\n";
    $output .= "<html lang=en>\n";
    $output .= "<head>\n";
    $output .= "<meta charset=utf-8>\n";
    $output .= "<title>".$application." ".$version." - ".$company." - ".$website."</title>\n";


    // =================================================================
    // Specify the CSS styles.
    // =================================================================

    // Add all CSS styles to a variable.
    $css = "

      html {
        box-sizing: border-box;
        height: 100%;
      }

      *,
      *:before,
      *:after {
        box-sizing: inherit;
      }

      body {
        background: ".$background.";
        font-family: Arial,sans-serif;
        font-weight: bold;
        font-size: 60px;
        margin: 0;
        min-height: 100%;
        padding-bottom: 58px;
        position: relative;
      }

      a {
        text-decoration: none;
      }

      a:hover {
        color: white;
      }

      .title {
        color: white;
        display: none;
        min-width: ".$min_width."px;
        text-align: center;
        width: 100%;
      }

      #search {
        background-color: transparent;
        color: white;
        font-size: 60px;
        margin: 0;
        min-width: ".$min_width."px;
        padding: 40px 0;
        width: 100%;
      }

      .content {
        margin: 0 auto;
        text-align: center;
        width: 100%;
      }

      .input-container {
        padding:0 0 10px 0;
        text-align: center;
      }

      input {
        border: none;
        color: #040515;
        font-size: 60px;
        margin-top: 10px;
        padding: 10px;
        text-align: center;
      }

      .name {
        background: #baffbc;
        width: 98%;
      }

      .url {
        background: #baffbc;
        width: 98%;
      }

      .save {
        background: #5dd262;
        padding: 10px 20px;
        width: 98%;
      }

      #links-container {
        text-align: center;
        margin-bottom: 200px;
      }

      .link-box {
        background: #cce3ff;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        display: inline-block;

        /* The following margin is only used when the linebreaks are */
        /* not removed from the HTML code. */
        /* margin-left: -4px; */

        margin-top: 1px;
        min-width: ".$min_width."px;
        outline: 1px solid #040515;
        text-align: center;
        width: 100%;
      }

      .link-box:hover {
        background: #6d97ca;
      }

      .link {
        color: #040515;
        display: inline-block;
        padding: 40px 0;
        width: 100%;
      }

      .footer {
        background-color: #0b0d29;
        bottom: 0;
        color: white;
        left: 0;
        padding: 10px 0;
        position: absolute;
        right: 0;
      }

      .left, .center, .right, .full {
        display: inline-block;
        text-align: center;
        width: 100%;
      }

      .left {
        min-width: 200px;
        padding: 10px 25px;
      }

      .right {
        min-width: 255px;
        padding: 10px 25px;
      }

      .l-link {
        color: white;
      }

      .c-link, .f-link, .r-link {
        color: white;
        display: inline-block;
      }

      .c-link {
        margin: 10px 30px;
      }

      .f-link {
        margin: 10px 30px;
      }

      @media (min-device-width: 500px) {
        .name,
        .url,
        .save {
          margin-left: 5px;
          margin-right: 5px;
        }

        input {
          font-size: 20px;
        }

        .name {
          width: 200px;
        }

        .url {
          width: 300px;
        }

        .save {
          width: unset;
        }
      }

      @media (min-device-width: 768px) {
        .left {float: left; text-align: left; width: 34%;}
        .center {width: 32%;}
        .right {float: right; text-align: right; width: 34%;}
        .c-link {margin: 10px 10px;}
      }

      @media (min-device-width: 1224px) {
        .left {width: 25%;}
        .center {width: 50%;}
        .right {width: 25%;}
        .c-link {margin: 10px 30px;}
      }

      @media (min-device-width: ".($min_width * 2)."px) {
        body {font-size: 16px;}
        .title, #search {font-size: 26px;}
        .link-box, .title {width: 50%;}
        .title {display: inline-block;}
        .link {padding: 15px 0;}
        #links-container {margin-bottom: 0;}
        #search {width: 50%; padding: 15px 0;}
      }

      @media (min-device-width: ".($min_width * 3)."px) {
        .link-box, .title {width: 33.3333%;}
        #search {width: 66.6666%; padding: 15px 0}
      }

      @media (min-width: ".($min_width * 4)."px) {
        body {font-size: 15px;}
        .title, #search {font-size: 20px;}
        .link-box, .title {width: 25%;}
        #search {width: 75%;}
      }

      @media (min-width: ".($min_width * 6)."px) {
        .link-box, .title {width: 16.6666%;}
        #search {width: 83.3333%;}
      }

      @media (min-width: ".($min_width * 8)."px) {
        body {font-size: 14px;}
        .link-box, .title {width: 12.5%;}
        #search {width: 87.5%;}
      }

      @media (min-width: ".($min_width * 10)."px) {
        .link-box, .title {width: 9.9999%;}
        #search {width: 89.9999%;}
      }

    ";


    // =================================================================
    // Minimize the CSS styles.
    // =================================================================

    // Remove all CSS comments.
    $css = preg_replace('/((?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:(?<!:)\/\/.*))/m', '', $css);

    // Replace multiple tabs, whitespaces and linebreaks with one whitespace.
    $css = preg_replace("/[\r\n]+/", " ", $css);
    $css = preg_replace("/\s+/", " ", $css);

    // Remove whitespaces.
    $css = str_replace(" {", "{", $css);
    $css = str_replace("{ ", "{", $css);
    $css = str_replace(" }", "}", $css);
    $css = str_replace("} ", "}", $css);
    $css = str_replace(" ;", ";", $css);
    $css = str_replace("; ", ";", $css);
    $css = str_replace(" :", ":", $css);
    $css = str_replace(": ", ":", $css);
    $css = str_replace(" >", ">", $css);
    $css = str_replace("> ", ">", $css);
    $css = str_replace(" ,", ",", $css);
    $css = str_replace(", ", ",", $css);

    // Remove unnecessary ';' chars.
    $css = str_replace(";}", "}", $css);


    // =================================================================
    // Specify the JS code.
    // =================================================================

    // Add all JS code to a variable.
    $js = "

      function search_items() {

        // Declare the variables.
        var input, filter, container, item, a, i, linkText;

        // Get the searchterm.
        input = document.getElementById('search');

        // Get the filter (uppercase searchterm).
        filter = input.value.toUpperCase();

        // Get the item container.
        container = document.getElementById('links-container');

        // Get all the link items.
        item = container.getElementsByClassName('link-box');

        // Loop through all the link items.
        for (i = 0; i < item.length; i++) {

          // Get the link of the actual item.
          a = item[i].getElementsByTagName('a')[0];

          // Get the text out of the link.
          linkText = a.textContent || a.innerText;

          // Check if the actual link text matches the search filter.
          if (linkText.toUpperCase().indexOf(filter) > -1) {

            // Don't hide the item.
            item[i].style.display = '';

            // The link text does not match the search filter.
          } else {

            // Hide the item.
            item[i].style.display = 'none';
          }
        } // Loop through all the link items.
      }

    ";

    // Remove all JS comments.
    $js = preg_replace('%//.*\n%i', '', $js);

    // Replace multiple tabs, whitespaces and linebreaks with one whitespace.
    $js = preg_replace("/[\r\n]+/", " ", $js);
    $js = preg_replace("/\s+/", " ", $js);

    // Remove whitespaces.
    $js = str_replace(" {", "{", $js);
    $js = str_replace("{ ", "{", $js);
    $js = str_replace(" }", "}", $js);
    $js = str_replace("} ", "}", $js);
    $js = str_replace(" ;", ";", $js);
    $js = str_replace("; ", ";", $js);
    $js = str_replace(" >", ">", $js);
    $js = str_replace("> ", ">", $js);
    $js = str_replace(" <", "<", $js);
    $js = str_replace("< ", "<", $js);
    $js = str_replace(" ,", ",", $js);
    $js = str_replace(", ", ",", $js);
    $js = str_replace(" =", "=", $js);
    $js = str_replace("= ", "=", $js);
    $js = str_replace(" (", "(", $js);
    $js = str_replace("( ", "(", $js);
    $js = str_replace(" )", ")", $js);
    $js = str_replace(") ", ")", $js);
    $js = str_replace(" |", "|", $js);
    $js = str_replace("| ", "|", $js);


    // =================================================================
    // Add the CSS styles to the output.
    // =================================================================

    // Add the CSS code to the HTML output.
    $output .= "<style>\n";
    $output .= trim($css);
    $output .= "</style>\n";

    // Add the JS code to the HTML output.
    $output .= "<script>\n";
    $output .= trim($js);
    $output .= "</script>\n";

    // Close the header part.
    $output .= "</head>\n";


    // =================================================================
    // Add the HTML body to the output.
    // =================================================================

    // Add the HTML body to the HTML output.
    $output .= "<body>\n";

    // Add the content container to the HTML output.
    $output .= "<div class=content>\n";


    // =================================================================
    // Add the input form or search field to the output.
    // =================================================================

    // Check if a new link should be added.
    if ($add == 1 && $enable_add == 1) {

      // Insert the input form.
      $output .= "<div class=input-container>\n";
      $output .= "<form action=\"".htmlspecialchars($_SERVER['PHP_SELF'])."\" method=post>\n";
      $output .= "<input class=name name=name placeholder=Name>\n";
      $output .= "<input class=url name=url placeholder=URL>\n";
      $output .= "<input class=save type=submit value=Save>\n";
      $output .= "<input type=hidden name=pass value=\"".$pass."\">\n";
      $output .= "</form>\n";
      $output .= "</div>\n";

      // Check if the search form should be displayed.
    } elseif ($search == 1) {

      // Add the search input field to the output.
      $output .= "<div class=title>\n";
      $output .= $application." ".$version;
      $output .= "</div>\n";

      $output .= "<input id=search onkeyup=search_items() placeholder=\"Search in ".count($links)." links...\">\n";
    }


    // =================================================================
    // Add the links container to the output.
    // =================================================================

    // Add the links container to the HTML output.
    $output .= "<div id=links-container>\n";

    // Loop through all the available links.
    foreach ($links as $name => $url) {

      // Check if a link should be deleted.
      if ($delete == 1 && $enable_delete == 1) {

        // Customize the URL for the delete process.
        $url = htmlspecialchars($_SERVER['PHP_SELF'])."?delete=1&name=".urlencode($name)."&url=".urlencode($url)."&pass=".$pass;

        // Specify the link target.
        $target = "_self";
      }


      // ===============================================================
      // Specify the alt text for the links.
      // ===============================================================

      // Search for whitespaces in the name.
      $position = strpos($name, " ");

      // Check if no whitespaces were found.
      if ($position === false) {

        // Specify the alt text without quotation marks.
        $alt_text = htmlspecialchars($name, ENT_QUOTES);

        // There were whitespaces in the name.
      } else {

        // Specify the alt text with additional quotation marks.
        $alt_text = "\"".htmlspecialchars($name, ENT_QUOTES)."\"";
      }


      // ===============================================================
      // Add the link box to the links container.
      // ===============================================================

      // Add the actual link box to the HTML output.
      $output .= "<div class=link-box>\n";
      $output .= "<a class=link href=\"".$url."\" title=".$alt_text." target=".$target.">\n";
      $output .= htmlspecialchars($name, ENT_QUOTES)."\n";
      $output .= "</a>\n";
      $output .= "</div>\n";
    }

    // Close the links container.
    $output .= "</div>\n";


    // =================================================================
    // Add the footer to the output - Left box.
    // =================================================================

    // Add the footer to the HTML output.
    $output .= "<div class=footer>\n";

    // Check if the copyright should be displayed.
    if ($display_copyright == 1) {

      // Specify the style for the centered box.
      $div_style = "center";

      // Specify the style for the centered links.
      $a_style = "c-link";

      // Display the left box with the number of links.
      $output .= "<div class=left>\n";
      $output .= "<a class=l-link href=\"".htmlspecialchars($_SERVER['PHP_SELF'])."?pass=\">\n";
      $output .= count($links)." Links\n";
      $output .= "</a>\n";
      $output .= "</div>\n";

      // The copyright should not be displayed.
    } else {

      // Specify the style for the centered box.
      $div_style = "full";

      // Specify the style for the centered links.
      $a_style = "f-link";
    }


    // =================================================================
    // Add the footer to the output - Centered box.
    // =================================================================

    // Add the centered box to the HTML output.
    $output .= "<div class=".$div_style.">\n";

    // Check if the 'delete' or 'new/add' mode is enabled.
    if ($enable_delete == 1 || $enable_add == 1) {

      // Display the link for the 'normal' mode.
      $output .= "<a class=".$a_style." href=\"".htmlspecialchars($_SERVER['PHP_SELF'])."?pass=".urlencode($pass)."\">\n";
      $output .= "Normal\n";
      $output .= "</a>\n";
    }

    // Check if the 'delete' mode is enabled.
    if ($enable_delete == 1) {

      // Display the link for the 'delete' mode.
      $output .= "<a class=".$a_style." href=\"".htmlspecialchars($_SERVER['PHP_SELF'])."?delete=1&pass=".urlencode($pass)."\">\n";
      $output .= "Delete\n";
      $output .= "</a>\n";
    }

    // Check if the 'new/add' mode is enabled.
    if ($enable_add == 1) {

      // Display the link for the 'new/add' mode.
      $output .= "<a class=".$a_style." href=\"".htmlspecialchars($_SERVER['PHP_SELF'])."?add=1&pass=".urlencode($pass)."\">\n";
      $output .= "New\n";
      $output .= "</a>\n";
    }

    // Check if the backup link is enabled.
    if ($enable_backup == 1) {

      // Get the parts of the script path.
      $path_parts = pathinfo($_SERVER['REQUEST_URI']);

      // Check if the script is in not the root of the web directory.
      if (trim($path_parts['dirname']) != DIRECTORY_SEPARATOR) {

        // Add the script path to the data file.
        $backup_file = $path_parts['dirname']."/".$data_file;

        // The script is in the root of the web directory.
      } else {

        // Only specify the filename of the data file.
        $backup_file = $data_file;
      }

      // Display the backup link.
      $output .= "<a class=".$a_style." href=\"".$backup_file."\" target=_blank>\n";
      $output .= "Backup\n";
      $output .= "</a>\n";
    }

    // Close the centered box.
    $output .= "</div>\n";


    // =================================================================
    // Add the footer to the output - Right box.
    // =================================================================

    // Check if the copyright should be displayed.
    if ($display_copyright == 1) {

      // Display the right box with the copyright.
      $output .= "<div class=right>\n";
      $output .= "Copyright ".date("Y")." by ";
      $output .= "<a class=r-link href=\"".$website."\" target=_blank>\n";
      $output .= htmlspecialchars($company, ENT_QUOTES);
      $output .= "</a>\n";
      $output .= "</div>\n";
    }

    // Close the footer.
    $output .= "</div>\n";

    // Close the content container.
    $output .= "</div>\n";

    // Close the HTML body.
    $output .= "</body>\n";
    $output .= "</html>\n";

    // Remove the linebreaks in the HTML code.
    $output = str_replace("\n", "", $output);

    // Display the generated output.
    echo $output;

    // The JSON data file could not be read.
  } else {

    // Display an error message.
    echo "The data file could not be read!";
  }

  // The JSON data file is not writable.
} else {

  // Display an error message.
  echo "No permissions to create/write the data file!";
}


// =====================================================================
// Function to save the data to the JSON file.
// =====================================================================

// Function needs the 'links' array and name of the data file as input.
function save_data($links, $data_file, $sort_links = 1) {

  // Check if the links array should be sorted.
  if ($sort_links == 1) {

    // Sort the array alphabetically.
    ksort($links);
  }

  // Encode the data into the JSON format.
  $json_data = json_encode($links);

  // Check if the JSON data file exist.
  if (file_exists($data_file)) {

    // Check if the JSON data file is writable.
    if (is_writable($data_file)) {

      // Write the data into the JSON file and check the result.
      if (file_put_contents($data_file, $json_data)) {

        // Set the status to success.
        $status = 0;

        // The data could not be saved.
      } else {

        // Set the status to error.
        $status = 1;
      }

      // The file is not writable.
    } else {

      // Set the status to error.
      $status = 2;
    }

    // The JSON data file does not exist.
  } else {

    // Set the status to error.
    $status = 3;
  }

  // Return the status.
  return $status;
}


// =====================================================================
// Function to read the data of the JSON file.
// =====================================================================

// Function needs the JSON data file as input.
function read_data($data_file, $sort_links = 1) {

  // Initialize the result variable.
  $result = FALSE;

  // Read the JSON data from the data file.
  $json_data = file_get_contents($data_file);

  // Check if the file could be read.
  if ($json_data !== false) {

    // Decode the JSON data and get an array.
    $links = json_decode($json_data, TRUE);

    // Check if the links array should be sorted.
    if ($sort_links == 1) {

      // Sort the array alphabetically.
      ksort($links);
    }

    // Specify the links array as result.
    $result = $links;
  }

  // Return the status.
  return $result;
}

?>
